//
//  potocol.swift
//  demo
//
//  Created by hjj on 2016/12/5.
//  Copyright © 2016年 hjj All rights reserved.
//


import Foundation
import UIKit

protocol loadPlist {
    
    func loadPlist() ->[[String: String]];
}

extension loadPlist {
    
    func loadPlist() ->[[String: String]]{
        let path = Bundle.main.path(forResource: "heros.plist", ofType: nil);
        return NSArray(contentsOfFile: path!) as! [[String: String]]

    }
}

protocol presentTable {
    
    func presentIcon(imageView: UIImageView);
    func presentName(label: UILabel);
    func presentIntro(label: UILabel);
}

struct ViewModel: presentTable {
    let name: String!
    let intro: String!
    let icon: String!
    
    init(model:Model){
        self.icon = model.icon
        self.intro = model.intro
        self.name = model.name
    }
    
    func presentIcon(imageView: UIImageView) {
        imageView.image = UIImage(named: self.icon)
        
    }
    func presentName(label: UILabel) {
        label.text = self.name
    }
    func presentIntro(label: UILabel) {
        label.text = self.intro
    }
}

struct Model {
    let name: String!
    let intro: String!
    let icon: String!
    init(_ dict: [String: String]) {
        self.icon = dict["icon"]!
        self.name = dict["name"]!
        self.intro = dict["intro"]!
    }
}
