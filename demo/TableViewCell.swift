//
//  TableViewCell.swift
//  demo
//
//  Created by hjj on 2016/12/5.
//  Copyright © 2016年 hjj All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var intro: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func present(viewModel: presentTable) {
        viewModel.presentIcon(imageView: icon)
        viewModel.presentIntro(label: intro)
        viewModel.presentName(label: name)
    }
    
}
