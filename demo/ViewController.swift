//
//  ViewController.swift
//  demo
//
//  Created by hjj on 2016/12/5.
//  Copyright © 2016年 hjj All rights reserved.
//


import UIKit


class ViewController: UITableViewController, loadPlist {

    
    var models: [[String: String]]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        models = loadPlist()
        
        
    }

}
extension ViewController{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TableViewCell
        if cell == nil {
            cell = Bundle.main.loadNibNamed("TableViewCell", owner: nil, options: nil)?.first as? TableViewCell
        }
        let viewModel = ViewModel(model: Model(models[indexPath.row]))
        
        cell?.present(viewModel: viewModel)
        return cell!
    }
    
}

